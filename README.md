# Tic-Tac-Toe

In this project, I worked with Oui Wein Jien to create an AI that plays Tic-Tac-Toe. The main dataset was built in the following format:
 - each step makes up a row in features and a step consists of the status of all 9 grids (e.g. [1, -1, 0, 0, -1, 1, 0, 0, 0])
 - outcome depends on whether the player wins in the game, as well as which step number it is in a game
 - if the player wins, the outcome will be a positive number, if it loses, it will have a negative number
 - the closer the step is to the end of a game, the more impact it has on the game, therefore, steps closer to the end of the game will have outcomes closer to 1, or -1, if it's not a draw
 - to be specific, the outcome value is computed as follows:
   - _gameOutcome_ * (pow(_a_,(_j_ + 1) / len(_features_)) - 1) / (_a_ - 1)
   - where _gameOutcome_ is -1, 0, or 1, for lose, draw and win
   - _a_ is a hyperparameter to be tuned
   - and _j_ is the step number

We first went with the linear regression approach and we achieved a winning rate of 68%. In order to have a better result, we then took the neural network approach, and achieved a winning rate of 88%, which was 20% higher.
